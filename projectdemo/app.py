import hashlib
import json
import os
import sys
from flask import Flask, request, jsonify, Response, make_response
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from flask_jwt_extended import create_access_token, JWTManager, create_refresh_token

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://backend_user:backend_user@localhost/postgres"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config["JWT_TOKEN_LOCATION"] = ["headers", "cookies", "json", "query_string"]
app.config["JWT_SECRET_KEY"] = "super-secret"
db = SQLAlchemy(app)

app.app_context().push()
jwt = JWTManager(app)

from models import User, UserToken


@app.route("/register", methods=['POST'])
def register_user():
    user_name = request.json.get('user_name')
    password = request.json.get('password')
    email = request.json.get('email')
    phone = request.json.get('phone')

    check_user_name = User.query.filter_by(username=user_name, is_delete=False).all()
    check_email = User.query.filter_by(email=email, is_delete=False).all()
    check_phone = User.query.filter_by(phone=phone, is_delete=False).all()

    if check_user_name or check_email or check_phone:
        return Response(json.dumps({
            "title": "Error",
            "messenger": "Username already exist" if check_user_name else (
                "Email already exist" if check_email else "Phone already exist"),
            "Error_Fields": [("user_name" if check_user_name else ("email" if check_email else "phone"))]
        }, indent=4), status=400)

    password = str(hashlib.md5(password.strip().encode('utf-8')).hexdigest())

    access_token = create_access_token('user_name')
    refresh_token = create_refresh_token('user_name')

    user = User(
        username=user_name,
        password=password,
        email=email,
        phone=phone
    )
    db.session.add(user)

    user_id = (User.query.filter_by(username=user_name).all())[0].id
    user_token = UserToken(
        user_id=user_id,
        token=access_token,
        refresh_token=refresh_token
    )

    db.session.add(user_token)
    db.session.commit()

    return Response(json.dumps({
        'title': 'Successfully!',
        'messenger': "Sign Up Success",
        'access_token': access_token,
        'refresh_token': refresh_token,
        'Error_Fields': []
    }, indent=4), status=200)


@app.route("/login", methods=['POST'])
def login():
    user_name = request.json.get('user_name')
    password = request.json.get('password')

    password = str(hashlib.md5(password.strip().encode('utf-8')).hexdigest())

    check_user = User.query.filter_by(username=user_name, password=password).all()

    if not check_user:
        Response(json.dumps({
            "title": "Error",
            "messenger": "Incorrect username or password",
            "Error_Fields": ['username', 'password']
        }, indent=4), status=400)

    if check_user[0].is_delete:
        Response(json.dumps({
            "title": "Error",
            "messenger": "User has been deleted",
            "Error_Fields": []
        }, indent=4), status=400)

    access_token = create_access_token('user_name')
    refresh_token = create_refresh_token('user_name')

    user_token = UserToken(
        user_id=check_user[0].id,
        token=access_token,
        refresh_token=refresh_token
    )

    db.session.add(user_token)
    db.session.commit()

    return Response(json.dumps({
        'title': 'Successfully!',
        'messenger': "Login Success",
        'access_token': access_token,
        'refresh_token': refresh_token,
        'Error_Fields': []
    }, indent=4), status=200)


@app.route("/updateuser", methods=['PATCH'])
def update_user():
    access_token = request.headers.get('Authorization')

    if not access_token:
        return Response(json.dumps({
            'title': 'Error!',
            'messenger': "Please login to do this",
            'Error_Fields': ['username', 'password']
        }, indent=4), status=400)

    user_id = (UserToken.query.filter_by(token=access_token).all())[0].user_id
    user = User.query.filter_by(id=user_id).all()

    new_email = request.json.get('email')
    new_phone = request.json.get('phone')
    new_password = request.json.get('password_new')

    if new_password:
        new_password = str(hashlib.md5(new_password.strip().encode('utf-8')).hexdigest())
        user[0].password = new_password

    if new_email:
        user[0].email = new_email

    if new_phone:
        user[0].phone = new_phone

    user_update = User()

    user_update.verified = True
    db.session.commit()

    return Response(json.dumps({
        'title': 'Successfully!',
        'messenger': "Update success",
        'Error_Fields': []
    }, indent=4), status=200)


@app.route('/deleteuser', methods=['DELETE'])
def delete_user():
    access_token = request.headers.get('Authorization')
    user_id = (UserToken.query.filter_by(token=access_token).all())[0].user_id
    user = User.query.filter_by(id=user_id).all()
    user[0].is_delete = True
    user_update = User()

    user_update.verified = True
    db.session.commit()

    return Response(json.dumps({
        'title': 'Successfully!',
        'messenger': "Delete user success",
        'Error_Fields': []
    }, indent=4), status=200)
