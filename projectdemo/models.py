from datetime import datetime

from sqlalchemy.orm import backref

from app import db


class User(db.Model):
    __tablename__ = 'users'
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(30), nullable=False)
    password = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(30), nullable=False)
    phone = db.Column(db.Integer(), nullable=False)
    is_delete = db.Column(db.Boolean(), default=False)


class UserToken(db.Model):
    __tablename__ = 'user_token'
    __table_args__ = {'extend_existing': True}
    user_id = db.Column(db.Integer, primary_key=True)
    token = db.Column(db.String(), nullable=False, primary_key=True)
    refresh_token = db.Column(db.String(), nullable=False)
    create_date = db.Column(db.DateTime, default=datetime.now())
